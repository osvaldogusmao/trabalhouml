package br.com.unifeob.porta;

public class Porta {

	private boolean portaEstaAberta = false;
	private boolean alarmeEstaAtivo = true;

	// Errado
	public boolean isPortaEstaAberta() {
		return portaEstaAberta;
	}

	public void setPortaEstaAberta(boolean portaEstaAberta) {
		this.portaEstaAberta = portaEstaAberta;
	}

	public boolean isAlarmeEstaAtivo() {
		return alarmeEstaAtivo;
	}

	public void setAlarmeAtivo(boolean alarmeEstaAtivo) {
		this.alarmeEstaAtivo = alarmeEstaAtivo;
	}
	
	// Certo
	public void fechaPorta(){
		portaEstaAberta = false;
	}
	
	public void abrePorta() {
		portaEstaAberta = true;
	}
	public boolean estaAberta(){
		return portaEstaAberta;
	}
	

}
