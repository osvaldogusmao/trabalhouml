package br.com.unifeob.main;

import br.com.unifeob.pessoa.Perna;
import br.com.unifeob.pessoa.Pessoa;

public class PessoaAndar {

	public static void main(String[] args) {

		// Certo
		Pessoa pessoa = new Pessoa();
		pessoa.andar();

		// Errado - segundo a Lei de Demeter
		for (Perna perna : pessoa.getPernas()) {
			if (!perna.andar()) {
				System.out.println("esquerda");
			}
			System.out.println("direita");
		}
	}
}