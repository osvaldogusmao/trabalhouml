package br.com.unifeob.main;

import br.com.unifeob.facade.ComputadorFacade;

public class MeuComputador {

	public static void main(String[] args) {
		ComputadorFacade facade = new ComputadorFacade();
		facade.iniciaComputador();
		facade.abreArquivo("arquivo.txt");
	}
	
}
