package br.com.unifeob.main;

import br.com.unifeob.porta.Porta;

/**
 * 
 * E se tiver que fechar a porta somente se o alarme estiver ativo?
 * Mostrar
 * 
 * */
public class AbrirPorta {

	public static void main(String[] args) {

		Porta porta = new Porta();

		//Errado
		/**
		 * 
		 * Codigo para fechar a porta
		 * 
		 * */
		if (porta.isPortaEstaAberta()) {
			porta.setPortaEstaAberta(false);
		}
		
		System.out.println("A porta esta aberta ? " + porta.isPortaEstaAberta());
		
		// Certo
		/**
		 * Codigo para fechar e abrir a porta
		 * 
		 * */
		porta.fechaPorta();
		
		System.out.println("A porta esta aberta ? " + porta.estaAberta());
		
		porta.abrePorta();
		
		System.out.println("A porta esta aberta ? " + porta.estaAberta());
		
	}

}
