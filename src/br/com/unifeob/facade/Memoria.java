package br.com.unifeob.facade;

public class Memoria {

	public byte[] carrega(long posicao, byte[] dados) {

		dados[0] = (byte) 01101010;
		dados[1] = (byte) 01010;

		System.out.println("Carregando o dados da posi��o " + posicao
				+ " sendo eles " + dados[(int) ConstantesComputador.POSICAO]);

		return dados;
	}
}
