package br.com.unifeob.facade;

/**
 * 
 * �Fornecer uma interface unificada para um conjunto de interfaces em um subsistema. 
 * Facade define uma interface de n�vel mais alto que torna o subsistema mais f�cil de ser usado.�
 * 
 * */
public class ComputadorFacade {
	
	private CPU cpu;
	private Memoria memoria;
	private HD hd;
	
	public ComputadorFacade() {
		this.cpu = new CPU();
		this.memoria = new Memoria();
		this.hd = new HD();
	}
	
	public void iniciaComputador(){
		preparaCPU(ConstantesComputador.POSICAO, ConstantesComputador.ENDERECO_BOOT, ConstantesComputador.TAMANHO);
	}
	
	public void abreArquivo(String arquivo){
		System.out.println("Preparando para ler arquivo");
		preparaCPU(ConstantesComputador.POSICAO_DOIS, ConstantesComputador.ENDERECO_BOOT, ConstantesComputador.TAMANHO);
		System.out.println("Finalizando leitura de arquivo");
	}
	
	private void preparaCPU(long posicao, long enderecoBoot, long tamanho){
		cpu.liberar();
		memoria.carrega(enderecoBoot, hd.le(posicao, (int) tamanho));
		cpu.vaiAte(posicao);
		cpu.executa();
	}
	
}
