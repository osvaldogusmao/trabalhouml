package br.com.unifeob.facade;

public class HD {

	public byte[] le(long setor, int tamanho){
		
		byte[] dados = new byte[10];
		
		System.out.println("Lendo os dados do setor " + setor + " tamanho " + tamanho);
		
		return dados;
	}
	
}
