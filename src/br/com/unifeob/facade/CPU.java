package br.com.unifeob.facade;

public class CPU {

	public void liberar() {
		System.out.println("liberando CPU");
	}

	public void vaiAte(long posicao) {
		System.out.println("indo at� a posi��o " + posicao);
	}

	public void executa() {
		System.out.println("Executando requisi��o");
	}
}
