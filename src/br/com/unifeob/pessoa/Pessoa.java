package br.com.unifeob.pessoa;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {

	private List<Perna> pernas = new ArrayList<Perna>();
	
	//Certo
	public boolean andar() {
		for (Perna perna : pernas) {
			if (!perna.andar()) {
				return false;
			}
		}
		return true;
	}
	
	
	//Errado
	public List<Perna> getPernas() {
		return pernas;
	}

}
